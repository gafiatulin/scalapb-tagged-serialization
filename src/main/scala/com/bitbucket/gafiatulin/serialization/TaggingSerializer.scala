package com.bitbucket.gafiatulin.serialization

import java.lang.reflect.Method
import java.nio.ByteBuffer
import java.util.concurrent.ConcurrentHashMap

import akka.serialization.{ByteBufferSerializer, SerializerWithStringManifest}
import com.google.protobuf.CodedInputStream

import scalapb.GeneratedMessage

/**
  * Created by victor on 20/10/2016.
  */

class TaggingSerializer extends SerializerWithStringManifest with ByteBufferSerializer{

  private final val USAGE_EXCEPTION =
    new IllegalArgumentException("Can't use TaggingSerializer with objects, which are not subclasses of com.trueaccord.scalapb.GeneratedMessage")

  private final val cisClazz = classOf[CodedInputStream]

  private final val parsingMethodBinding = new ConcurrentHashMap[String, Method]

  private final def wrap(message: GeneratedMessage): ProtobufMeassageEnvelope = {
    val clazz = message.getClass
    ProtobufMeassageEnvelope(message.toByteString, clazz.getPackage.getName, clazz.getSimpleName)
  }

  private final def unwrap(envelope: ProtobufMeassageEnvelope): AnyRef = {
    val canonicalName = envelope.packageName ++ "." ++ envelope.message
    val clazz = Class.forName(canonicalName)
    if (classOf[GeneratedMessage].isAssignableFrom(clazz)){
      parsingMethodBinding.computeIfAbsent(canonicalName, _ => clazz.getDeclaredMethod("parseFrom", cisClazz))
        .invoke(null, envelope.bytes.newCodedInput)
    } else {
      throw USAGE_EXCEPTION
    }
  }

  override def identifier: Int = 353432

  override def manifest(o: AnyRef): String = o match {
    case _: GeneratedMessage => o.getClass.getCanonicalName
    case _ => throw USAGE_EXCEPTION
  }

  override def toBinary(o: AnyRef, buf: ByteBuffer): Unit = o match {
    case message: GeneratedMessage => wrap(message).toByteString.copyTo(buf)
    case _ => throw USAGE_EXCEPTION
  }

  override def toBinary(o: AnyRef): Array[Byte] = o match {
    case message: GeneratedMessage => wrap(message).toByteArray
    case _ => throw USAGE_EXCEPTION
  }

  override def fromBinary(buf: ByteBuffer, manifest: String): AnyRef =
    unwrap(ProtobufMeassageEnvelope.parseFrom(CodedInputStream.newInstance(buf)))

  override def fromBinary(bytes: Array[Byte], manifest: String): AnyRef =
    unwrap(ProtobufMeassageEnvelope.parseFrom(bytes))
}
